import {Button, Card} from 'react-bootstrap';

export default function Projects() {
    return(
        <section id="projects">
        <div>
            <h2 id="project" className='text-center'>Recent Projects</h2>
            <div className='d-flex justify-content-center
            flex-column flex-xl-row flex-lg-row flex-md-column flex-sm-column'>
            {/*==================== For MNLTour =============*/}
            <div className="projects">
                <Card className="proj-card mx-2 my-1" style={{ width: '23rem', height:'26rem' }}>
                    <Card.Img variant="top" src="https://drive.google.com/uc?export=view&id=1k0gXt2lOZfnAL4u2IIPoX6IuiUgQ_hIJ" />
                    
                    <Card.Body>
                        <Card.Title>MNLTour</Card.Title>
                            <Card.Text>
                            MNLTour is our college thesis topic. It is a mobile and web application for virtual tour of some of the well-known tourist spots in Manila such as Rizal Park and some part of Intramuros. 
                            </Card.Text>
                    {/* <Button className='proj-btn' variant="primary" disabled>Go somewhere</Button> */}
                    </Card.Body>
                </Card>
            </div>

            {/*================= E-commerce Website ===============*/}
            <div className="projects">
            <Card className="proj-card mx-2 my-1" style={{ width: '23rem', height:'26rem' }}>
                <Card.Img variant="top" src="https://drive.google.com/uc?export=view&id=1-VCP5otpT3hnBjUqn2PZ0bOzLY4C5ptR" />
                <Card.Body>
                    <Card.Title>E-commerce Website</Card.Title>
                        <Card.Text>
                        This project serves as my final capstone requirement on my bootcamp at Zuitt.co. MERN stack is used in building this project.
                        </Card.Text>
                <Button className='proj-btn'><a href="https://e-commerce-app-mu-virid.vercel.app/"  target="blank">Visit Website</a></Button>
                </Card.Body>
            </Card>
            </div>

            {/*============= Web Portfolio ================= */}
            <div className="projects">
                <Card className="proj-card mx-2 my-1" style={{ width: '23rem', height:'26rem' }}>
                    <Card.Img variant="top" src="https://drive.google.com/uc?export=view&id=1_nLWRlvjOrk4yBzlYZHkPxRJZZnRU-jy" />
                    <Card.Body>
                        <Card.Title>Web Portfolio</Card.Title>
                            <Card.Text>
                            This is also one of the capstone at Zuitt.co and my initial web portfolio before I learned React.js. This is purely front-end project that was created using using HTML, CSS and Bootstrap.
                            </Card.Text>
                    <Button className='proj-btn'><a href='https://rommel0228.github.io/portfolio/' target="_blank">Visit Website</a></Button>
                    </Card.Body>
                </Card>

            </div>
            </div>
        </div>
        <div className='mt-5'>
            <h2 className='text-center'>Tools I Have Knowledge About</h2>
            <div className='tools d-flex mx-auto
            flex-row flex-lg-column flex-xl-column flex-md-row flex-sm-row'>
                {/* Tools Icons - First Row */}
                <div className=' mx-auto mt-3
                    d-flex flex-column flex-sm-column flex-md-column flex-lg-row flex-xl-row
                    col-5 col-xl-10 col-lg-10 col-md-5 col-sm-5'>
                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5
                    '>  <img  className="container-fluid tools-img rounded-circle pr-1" src="https://cdn.worldvectorlogo.com/logos/mongodb-icon-1.svg" />
                    </div>

                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5'
                    >  <img  className=" container-fluid tools-img rounded-circle pr-1"src="https://cdn-icons-png.flaticon.com/512/919/919825.png" />
                    </div>

                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5
                    '>  <img  className=" container-fluid tools-img express rounded-circle pr-1" src="https://w7.pngwing.com/pngs/925/447/png-transparent-express-js-node-js-javascript-mongodb-node-js-text-trademark-logo.png" />
                    </div>
                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5
                    '>  <img  className=" container-fluid github tools-img rounded-circle pr-1" src="https://cdn.pixabay.com/photo/2022/01/30/13/33/github-6980894_960_720.png" />
                    </div>
                </div>

                {/* Tools Icons - Second Row */}
                <div className='mx-auto mt-3
                    d-flex flex-column flex-sm-column flex-md-column flex-lg-row flex-xl-row
                    col-5 col-xl-10 col-lg-10 col-md-5 col-sm-5'>
                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5'
                    >  <img  className=" container-fluid tools-img rounded-circle pr-1" src="https://ionicframework.com/docs/icons/logo-react-icon.png" />
                    </div>

                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5'
                    >  <img  className=" container-fluid tools-img rounded-circle pr-1" src="https://git-scm.com/images/logos/logomark-orange@2x.png" />
                    </div>

                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5'
                    >  <img  className=" container-fluid tools-img rounded-circle pr-1" src="https://uxwing.com/wp-content/themes/uxwing/download/brands-and-social-media/postman-icon.png" />
                    </div>
                    
                    <div className=' mx-auto
                        col-xl-2 col-lg-2 col-md-4 col-sm-5 col-5'
                    >  <img  className=" container-fluid tools-img rounded-circle pr-1" src="https://gitlab.com/uploads/-/system/project/avatar/278964/project_avatar.png" />
                    </div>
                </div>  

            </div>
        </div>
        
        </section>
    )
}