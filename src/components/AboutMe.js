export default function AboutMe() {
    return(
        <div className="min-vh-100 d-flex flex-column flex-md-row mx-auto col-10 ">
        
        {/* //For image */}
        <div className="img-container mx-auto my-auto rounded-circle
        col-xl-5 col-lg-5 col-md-5 col-sm-10 col-10">
            <img className="profile-img container-fluid my-auto rounded-circle " src="https://drive.google.com/uc?export=view&id=1mwrvTpymq0v0Ja7HN6aPdwnrlO89Xa29"/>
        </div>

        {/* //Introduction */}
        <div className="mx-auto  my-auto
        col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12"
        
        >
            <h2 className="text-justify">Hello! I am Rommel Esteves. <br/> 
                I am a Full-Stack Web Developer.</h2>.
            <p className="text-light">Download CV
                <a className="cv" href="https://drive.google.com/file/d/1Jv33R2xJTPsXeW7a4935c0m1QWT1g-R9/view?usp=share_link" target="_blank">
                    <img className="cv-icon"src="https://cdn.pixabay.com/photo/2016/12/18/13/45/download-1915753_1280.png"/>
                </a>
            </p>
        </div>
        </div>
    )
}