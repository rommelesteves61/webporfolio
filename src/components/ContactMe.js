import React, { useRef , useState} from 'react';
import emailjs from '@emailjs/browser';

import Swal from 'sweetalert2'

export default function ContactMe() {

  const [user_name, setName] = useState("");
  const [user_email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_jst2rns', 'template_ftlgohv', form.current, 'eHmKqAStsS-1rGaYO')
      .then((result) => {
          Swal.fire({
            icon: 'success',
            title: 'Successful!',
            text: 'Your message is successfully sent to Rommel'
          })
          
      }, (error) => {
          Swal.fire({
            icon: 'error',
            title: 'Something went wrong.',
            text: 'Please try again!'
          })
      });
      setName("");
      setEmail("");
      setMessage("");
  };

  return (
    <section id="contact" className='mt-5'>
      <h2 className='text-center'>Contact Me</h2>
      <div className='d-flex col-10 mx-auto
          flex-column flex-xl-row flex-lg-row flex-md-row flex-sm-column'>
        {/* Contact Form */}
        <div className='col-xs-12 col-sm-12 col-md-6'>
          <form className="d-flex flex-column col-12" ref={form} onSubmit={sendEmail}>
            <label className="text-light mt-3"><strong>Name</strong></label>
            <input
              className='mt-2'
              type="text"
              placeholder="Your name"
              value={user_name}
              name="user_name"
              onChange={(e) => setName(e.target.value)}
            />

            <label className="text-light mt-3"><strong>Email</strong></label>
            <input
              className='mt-2'
              type="email"
              placeholder="Your email"
              value={user_email}
              name="user_email"
              onChange={(e) => setEmail(e.target.value)}
            />
            <label className="text-light mt-3"><strong>Message</strong></label>
            <textarea
              className='mt-2'
              placeholder="Your message"
              value={message}
              name="message"
              onChange={(e) => setMessage(e.target.value)}
            />
            <button className="col-2 rounded mt-3 py-1 text-primary" type="submit"><strong>Send</strong></button>
          </form>
        </div>

        <br />
        <br />

        {/* Social Media Contacts */}
        <div className='d-flex m-auto justify-content-center col-xs-10'>
          {/* LinkedIn */}
          <div className='mx-3'>
            <a href='https://www.linkedin.com/in/rommel-esteves-8854b8148/' target="_blank">
                <img className='linked-logo'src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/LinkedIn_logo_initials.png/800px-LinkedIn_logo_initials.png"/>
            </a>
          </div>

          {/* Facebook */}
          <div className='mx-3'>
            <a href='https://www.facebook.com/esteves.rommel' target="_blank">
              <img className='fb-logo' src="https://www.freepnglogos.com/uploads/facebook-logo-icon/facebook-logo-clipart-flat-facebook-logo-png-icon-circle-22.png"/>
            </a>
          </div>
        </div>
      </div>
    </section>
  
    
  );
};