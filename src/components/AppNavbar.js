import { Container, Navbar, Nav } from "react-bootstrap";
import { Link } from 'react-scroll';

export default function AppNavbar(){
	
	return (
		<Navbar expand="lg" className="nav-bar">
			<Container fluid>
				<Navbar.Brand className="text-light"><strong>Rommel Esteves</strong></Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link className="text-light"><strong>About Me</strong></Nav.Link>
						<Nav.Link className="text-light">
							<Link to="projects"><strong>Projects</strong></Link>
						</Nav.Link>
            			<Nav.Link className="text-light">
							<Link to="contact"><strong>Contact Me</strong></Link>
						</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}

