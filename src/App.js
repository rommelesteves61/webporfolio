import './App.css';

import AppNavbar from './components/AppNavbar'
import AboutMe from './components/AboutMe'
import Projects from './components/Projects'
import ContactMe from './components/ContactMe'

function App() {
  return (
    <>
    <AppNavbar />
    <AboutMe />
    <Projects />
    <ContactMe />
    </>
  );
}

export default App;
